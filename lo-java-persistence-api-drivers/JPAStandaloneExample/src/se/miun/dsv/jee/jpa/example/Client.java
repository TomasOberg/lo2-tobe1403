package se.miun.dsv.jee.jpa.example;

import se.miun.dsv.jee.jpa.example.RMIPopulator.*;
import se.miun.dsv.jee.jpa.example.model.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;


/**
 * Client which calls through RMI-tech method which parses and all the events
 * found in a text file.
 */
public class Client {

	public static void main(String[] args) {

        //---------------------------------------------------------------------
        // Create a container for all events
        //---------------------------------------------------------------------
        List<Event> events = new ArrayList();

        //---------------------------------------------------------------------
        // RMI - call the method arseToEvents on the server side
        //---------------------------------------------------------------------
        try {
            String name = "jee16";
            Registry registry = LocateRegistry.getRegistry("localhost");
            RMIPopulatorInterface comp = (RMIPopulatorInterface) registry.lookup(name);

            String input = getStringFromURL("https://bitbucket.org/javaee2016/javaee16/raw/master/material/events.txt");
            events = comp.parseToEvents(input);

            for(Event event : events) {
                System.out.println(event.getTitle());
            }

        }
        catch (NotBoundException e) {
            throw new RuntimeException("Could not bind interface");
        }
        catch (RemoteException e1) {
            e1.printStackTrace();
        }
        catch (Exception e) {
            System.err.println("ComputePi exception:");
            e.printStackTrace();
        }

        //---------------------------------------------------------------------
        // Store all the events in the database using JPA
        //---------------------------------------------------------------------
        EventsAndUsersDAO dao = new EventsAndUsersDAO();
        dao.persistAll(events);
        dao.shutDown();
    }

    private static String getStringFromURL(String urlString) {
        //---------------------------------------------------------------------
        // Connect to the adress and read the string line for line
        //---------------------------------------------------------------------
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        StringBuilder input = new StringBuilder();
        try {
            assert url != null;
            Scanner scanner = new Scanner(url.openStream());
            while(scanner.hasNextLine()) {
                input.append(scanner.nextLine());
                input.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String userAndEventData = input.toString();


        /*
        //---------------------------------------------------------------------
        // TODO: Läs in från filen events.txt med hjälp av niobiblioteket
        //---------------------------------------------------------------------
        */
        /*
        Path file;
        file = new Paths.get("file:///events.txt");

        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(file, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }*/

        return userAndEventData;

    }
}
