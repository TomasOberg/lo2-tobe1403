package se.miun.dsv.jee.jpa.example;

import se.miun.dsv.jee.jpa.example.RMIPopulator.RMIPopulator;
import se.miun.dsv.jee.jpa.example.RMIPopulator.RMIPopulatorInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by tomasoberg on 2016-11-21.
 */
public class Server {

    public static void main(String[] args) {
        System.getProperties().setProperty("java.security.policy", "no.policy");
        System.getProperties().setProperty("java.rmi.server.hostname", "localhost");

        try {
            java.rmi.registry.LocateRegistry.createRegistry(1099);
            String name = "jee16";

            RMIPopulatorInterface populator = new RMIPopulator();
            RMIPopulatorInterface stub = (RMIPopulatorInterface) UnicastRemoteObject.exportObject( populator, 0 );
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);    // TODO: stub? på vilket sätt?

            System.out.println("RMIPopulator bound");
        }
        catch (Exception e) {
            System.err.println("RMIPopulator exception:");
            e.printStackTrace();
        }
    }
}
