package se.miun.dsv.jee.jpa.example.RMIPopulator;

/**
 * Created by tomasoberg on 2016-11-21.
 */
import se.miun.dsv.jee.jpa.example.model.Event;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Interface for
 */
public interface RMIPopulatorInterface extends Remote {
    public List<Event> parseToEvents(String userAndEventData) throws RemoteException;
}
