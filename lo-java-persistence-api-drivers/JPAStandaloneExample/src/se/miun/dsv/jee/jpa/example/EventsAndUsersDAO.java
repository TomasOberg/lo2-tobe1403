package se.miun.dsv.jee.jpa.example;

/**
 * Created by tomasoberg on 2016-11-21.
 */

import se.miun.dsv.jee.jpa.example.model.Event;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Set;

/**
 * A Data Access Object class implementation that hides all database specific code from the caller.
 *
 */
public class EventsAndUsersDAO {

    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("example");

    public void persistAll(List<Event> events) {
        EntityManager manager = entityManagerFactory.createEntityManager();

        EntityTransaction tx = manager.getTransaction();
        try {
            tx.begin();

            for (Event event : events)
                manager.persist(event);

            tx.commit();
        }
        catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            manager.close();
        }
    }

    public void shutDown() {
        entityManagerFactory.close();
    }
}